#ifndef __PARAMS__
#define __PARAMS__

#include "dependency/INIReader.h"
#include <iostream>

struct ConfigParams
{
	const bool DISPLAY_IMAGE;
	const bool DISPLAY_DEBUG_IMAGE;
	const bool DEBUG_MESSAGES;
	const int  FRAME_DIVIDE;
	const int  ADDAPTIVE_THRESH_CORRECTION;
	const int  CANNY_MAX_THREASHOLD;
	const int  HOUGH_PROX_RESOLUTION;
	const int  HOUGH_VOTE_THREASHOLD;
	const float HOUGH_MIN_RADIUS_PERCENT;
	const float VOLTAGE_PIXEL_CONVERSION;
	const uint8_t ADC_ADDRESS_1;
	const uint8_t ADC_ADDRESS_2;
	const uint8_t ADC_BITRATE;
	const uint8_t ADC_PGA;
	const uint8_t ADC_CONVERSION_MODE;
	const int MOTOR_X_STEP_PIN;
	const int MOTOR_X_DIR_PIN;
	const int MOTOR_Y_STEP_PIN;
	const int MOTOR_Y_DIR_PIN;
	const float VECTOR_MAG_CONVERSION;
	const float MANUAL_VECTOR_SENSITIVITY;
	const int MOTOR_PULSE_WIDTH_US;
	
	ConfigParams(const std::string &fileName);

};

extern const ConfigParams params;

int loadInt(const std::string &fileName, const std::string paramLoc, const std::string paramName, int defautlVal);

uint8_t loadUInt8(const std::string &fileName, const std::string paramLoc, const std::string paramName, int defautlVal);

float loadFloat(const std::string &fileName, const std::string paramLoc, const std::string paramName, float defautlVal);

bool loadBool(const std::string &fileName, const std::string paramLoc, const std::string paramName, bool defautlVal);



#endif