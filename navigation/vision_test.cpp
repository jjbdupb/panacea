#include "vision.hpp"
#include "params.hpp"
#include "pi_io/IO.hpp"
#include <iostream>
#include <fstream>
#include <unistd.h>

#define TIME_STEP 500


void log_reflection_data(cv::Vec4f vec){
	float c1, c2, c3, c4, c5, c6;
	c5 = vec[0] - vec[1];
	c6 = vec[2] - vec[3];

	std::fstream file;
	file.open("reflection_data.csv", std::ios::app);
	if(file.is_open()){
		file << c1 << ',' << c2 << ',' << c3 << ',' << c4 << ',' << c5 << ',' << c6 << "\n";
	}
	file.close();

}


int main(int argc, char const *argv[])
{

	std::cout << "press enter to start tests" << std::endl;
	std::string s;
	std::cin >> s;
	for(;;){
		cv::Vec4f ref_vector = read_reflection_voltage();
		log_reflection_data(ref_vector);
		usleep(TIME_STEP);
	}
	
	return 0;
}