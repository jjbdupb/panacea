#ifndef __REFLECTION__
#define __REFLECTION__

#include <iostream>
#include <opencv2/opencv.hpp>
#include "pi_io/IO.hpp"

cv::Point2f calibrate_reflection(cv::Point v);


cv::Point2f process_reflection(){
	cv::Vec4f voltages = read_reflection_voltage();
	float v_up = voltages[0] - voltages[1];
	float v_right = voltages[2] - voltages[3];

	cv::Point2f reflection_direction_vector = cv::Point2f(v_right, v_up);
	return calibrate_reflection(reflection_direction_vector);
}


cv::Point2f calibrate_reflection(cv::Point v){
	v.x = v.x * params.VOLTAGE_PIXEL_CONVERSION * -1;
	v.y = v.y * params.VOLTAGE_PIXEL_CONVERSION * -1;

	return v;
}

#endif