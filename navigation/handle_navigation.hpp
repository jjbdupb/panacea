#ifndef __HANDLE_NAVIGATION__
#define __HANDLE_NAVIGATION__

#include <iostream>
#include <opencv2/opencv.hpp>
#include "reflection.hpp"
#include "vision.hpp"
#include "params.hpp"
#include <algorithm>

extern const ConfigParams params;

cv::Point2f combine_vec(cv::Point p1, cv::Point2f p2);


cv::Point handle_navigation(cv::Mat frame){
	cv::Point vision_direction_vector = process_frame(frame);
	cv::Point2f reflection_direction_vector = process_reflection();
	cv::Point2f direction_vector = combine_vec(vision_direction_vector, reflection_direction_vector);
	return direction_vector;
}

cv::Point2f combine_vec(cv::Point p1, cv::Point2f p2){
	float p2_weight = std::max(p2.x, p2.y) / params.VOLTAGE_PIXEL_CONVERSION;
	float p1_weight = 1 - p2_weight;

	float x = (p1.x * p1_weight) + (p2.x * p2_weight);
	float y = (p1.y * p1_weight) + (p2.y * p2_weight);
	return cv::Point2f(x, y);
}

#endif
