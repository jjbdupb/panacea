#ifndef __VISION__
#define __VISION__
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include "params.hpp"

extern const ConfigParams params;

cv::Mat filter(cv::Mat frame);
cv::Vec3f get_average_circle(cv::Mat frame);
cv::Point get_differance_vector(cv::Mat frame, cv::Vec3f average_circle);
cv::Vec3f average_circles(std::vector<cv::Vec3f> circles);
cv::Point process_frame(cv::Mat frame);


cv::Point process_frame(cv::Mat frame){
	if (frame.empty()){
		std::cerr <<"ERROR - empty frame grabbed \n";
		exit(-1);
	}
	cv::Mat filtered = filter(frame);
	cv::Vec3f avg_circle = get_average_circle(filtered);
	cv::Point diff_vector = get_differance_vector(frame, avg_circle);

	if (params.DISPLAY_DEBUG_IMAGE){
		cv::imshow("filtered", filtered);

		cv::Mat edge;
		cv::Canny(filtered, edge, params.CANNY_MAX_THREASHOLD/2, params.CANNY_MAX_THREASHOLD);
		cv::imshow("edge", edge);

		cv::Point center = cv::Point(avg_circle[0], avg_circle[1]);
    	cv::circle(frame, center, 1, cv::Scalar(0,100,100), 3, cv::LINE_AA);
    	cv::circle(frame, center, avg_circle[2], cv::Scalar(255,0,255), 3, cv::LINE_AA);

    	cv::Point frame_centre = cv::Point(frame.cols /2, frame.rows/2);
    	cv::circle(frame,frame_centre, 1, cv::Scalar(0,0,255), 3, cv::LINE_AA);

  		cv::imshow("circ", frame);
  	}

	return diff_vector;

}

cv::Mat filter(cv::Mat frame){
	cv::cvtColor(frame, frame, cv::COLOR_BGR2GRAY);
	cv::GaussianBlur(frame, frame, cv::Size(9, 9), 2, 2);
	cv::adaptiveThreshold(frame, frame, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 7, params.ADDAPTIVE_THRESH_CORRECTION);
	cv::GaussianBlur(frame, frame, cv::Size(7, 7), 2, 2);
	return frame;
}

cv::Vec3f get_average_circle(cv::Mat frame){
	std::vector<cv::Vec3f> circles;
	cv::Vec3f circle;
	cv::HoughCircles(frame, circles, cv::HOUGH_GRADIENT,
		1, frame.rows/params.HOUGH_PROX_RESOLUTION, params.CANNY_MAX_THREASHOLD, params.HOUGH_VOTE_THREASHOLD, frame.rows*params.HOUGH_MIN_RADIUS_PERCENT);
	if (circles.size() > 0){
		std::cout << "num circs " << circles.size() << std::endl;
		circle = average_circles(circles);
	} else {
		std::cout << "No Circs detected" << std::endl;
		circle = cv::Vec3f(frame.cols/2, frame.rows/2, 1);
	}
	return circle;
}


cv::Point get_differance_vector(cv::Mat frame, cv::Vec3f average_circle){
	int x_diff = average_circle[0] - (frame.cols / 2);
	int y_diff = (frame.rows / 2) - average_circle[1];
	return cv::Point(x_diff, y_diff);
}


cv::Vec3f average_circles(std::vector<cv::Vec3f> circles){
	cv::Vec3f avg_circle;
	for(size_t i = 0; i < circles.size(); i++){
		avg_circle[0] += circles[i][0];
		avg_circle[1] += circles[i][1];
		avg_circle[2] += circles[i][2];
	}
	avg_circle[0] = avg_circle[0]/circles.size();
	avg_circle[1] = avg_circle[1]/circles.size();
	avg_circle[2] = avg_circle[2]/circles.size();

	return avg_circle;
}


#endif