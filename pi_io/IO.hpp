#ifndef __PI_IO__
#define __PI_IO__

#include <iostream>
#include <opencv2/opencv.hpp>
#include <pigpio.h>
#include <cmath>
#include <fstream>
#include <cfloat>
#include <cstdio>
#include <sstream>
#include <unistd.h>
#include "dependency/ADCPi/ABE_ADCPi.h"
#include "params.hpp"

#define MAX_ANGLE 30
#define ANGLE_STEP 0.8
#define LOCATION_FILE_LOC "/etc/panacea.loc"
#define TEMP_LOC_FILE "/tmp/pan_tmp.loc"
#define JOYSTICK_VOLTAGE_MIDPOINT 2.5

extern const ConfigParams params;


void init_IO(){
	int init = gpioInitialise();
	if(init<0){
		std::cerr<<"ERROR GPIO FAILED TO INITALISE" << std::endl;
		exit(-1);
	}

	gpioSetMode(params.MOTOR_X_DIR_PIN, PI_OUTPUT);
	gpioSetMode(params.MOTOR_X_STEP_PIN, PI_OUTPUT);
	gpioSetMode(params.MOTOR_Y_STEP_PIN, PI_OUTPUT);
	gpioSetMode(params.MOTOR_Y_DIR_PIN, PI_OUTPUT);
}


float get_current_posistion(int motorStepPin){
	std::fstream file;
	float pos = FLT_MAX;
	std::string line;
	file.open(LOCATION_FILE_LOC, std::ios::in);
	if(file.is_open()){
		while(std::getline(file, line)){
			if(line.rfind(std::to_string(motorStepPin),0) == 0){
				int epos = line.find(" ");
				line.erase(0, epos);
				pos = std::stof(line);
				break;
			}
		}
	}else{
		std::cout << "Possition file not found, assuming position zero" << std::endl;
		file.open(LOCATION_FILE_LOC, std::ios::out|std::ios::app);
		file << motorStepPin << " " << 0.0f << "\n";
		pos = 0.0f;
	}

	file.close();

	if(pos == FLT_MAX){
		std::cout << "Motor num not found in possition file, assuming position zero" << std::endl;
		std::fstream appfile;
		appfile.open(LOCATION_FILE_LOC, std::ios::app);
		appfile << motorStepPin << " " << 0.0f << "\n";
		pos = 0.0f;
		appfile.close();
	}

	return pos;
}

void save_motor_posision(int motorStepPin, float fpos){
	std::fstream file;
	std::ofstream outfile(TEMP_LOC_FILE);
	std::string line;
	bool written = false;
	file.open(LOCATION_FILE_LOC);
	if(file.is_open()){
		while(std::getline(file, line)){
			if(line.rfind(std::to_string(motorStepPin), 0) == 0){
				std::ostringstream oss;
				oss << motorStepPin << " " << fpos;
				line = oss.str();
				written =  true;
			}
			outfile << line <<'\n';

		}
	}else{
		std::cout << "Could not open possition file ... generating new one" << std::endl;
		std::ostringstream oss;
		oss << motorStepPin << " " << fpos << "\n";
		line = oss.str();
		outfile << line;
	}

	outfile.close();
	file.close();
	if(!written){
		std::cout << "Could not find motor num in possition file ... generating" << std::endl;
		std::fstream appfile;
		appfile.open(TEMP_LOC_FILE, std::ios::app);
		appfile << motorStepPin << " " << fpos << "\n";
		appfile.close();
	}
	std::rename(TEMP_LOC_FILE, LOCATION_FILE_LOC);
	std::remove(TEMP_LOC_FILE);
}


void step_motors(int xn, int xdir, int xmotorStepPin, int xmotorDirPin, int yn, int ydir, int ymotorStepPin, int ymotorDirPin){
	gpioWrite(xmotorStepPin, 0);
	gpioWrite(ymotorStepPin, 0);
	gpioWrite(xmotorDirPin, xdir);
	gpioWrite(ymotorDirPin, ydir);

	if(xdir == 0){
		xdir = -1;
	}

	if(ydir == 0){
		ydir = -1;
	}

	float xcpos = get_current_posistion(xmotorStepPin);
	float xfpos = xcpos + xdir*xn*ANGLE_STEP;
	float ycpos = get_current_posistion(ymotorStepPin);
	float yfpos = ycpos + ydir*yn*ANGLE_STEP;
	if(abs(xfpos) > MAX_ANGLE){
		xn = abs(floor((xdir*MAX_ANGLE-xcpos)/ANGLE_STEP));
		std::cout<<"Tried to move beyond max angle, CLAMPING" << std::endl;
		xfpos = xdir*MAX_ANGLE;
	}

	if(abs(yfpos) > MAX_ANGLE){
		yn = abs(floor((ydir*MAX_ANGLE-ycpos)/ANGLE_STEP));
		std::cout<<"Tried to move beyond max angle, CLAMPING" << std::endl;
		yfpos = ydir*MAX_ANGLE;
	}



	for (int i = 0; i < std::max(xn, yn); ++i)
	{
		if (i < xn){
			gpioWrite(xmotorStepPin, 1);
		}
		if(i < yn){
			gpioWrite(ymotorStepPin, 1);
		}
		usleep(params.MOTOR_PULSE_WIDTH_US);
		gpioWrite(xmotorStepPin, 0);
		gpioWrite(ymotorStepPin, 0);
		usleep(params.MOTOR_PULSE_WIDTH_US);
	}

	save_motor_posision(xmotorStepPin, xfpos);
	save_motor_posision(ymotorStepPin, yfpos);

	if(params.DEBUG_MESSAGES){
		std::cout << "Motor " << xmotorStepPin << " moved to " << xfpos <<std::endl;
		std::cout << "Motor " << ymotorStepPin << " moved to " << yfpos <<std::endl;
	}
	gpioWrite(xmotorStepPin, 0);
	gpioWrite(ymotorStepPin, 0);

}


void handle_movement(cv::Point2f movement_vector, bool assisted){
	float x_move;
	float y_move;
	int x_steps;
	int y_steps;
	if(assisted){
		x_move = movement_vector.x * params.VECTOR_MAG_CONVERSION;
		y_move = movement_vector.y * params.VECTOR_MAG_CONVERSION;
	}else{
		x_move = movement_vector.x * params.MANUAL_VECTOR_SENSITIVITY;
		y_move = movement_vector.y * params.MANUAL_VECTOR_SENSITIVITY;
	}
	x_steps = round(x_move);
	y_steps = round(y_move);
	if(params.DEBUG_MESSAGES){
		std::cout << "X_move:" << x_move << " Y_move " << y_move << std::endl;
		std::cout << "X steps: " << x_steps << " Y steps: " << y_steps << std::endl;
	}

	int xdir;
	int ydir;
	if(x_steps > 0){xdir=1;}else{xdir=0;}
	if(y_steps>0){ydir=1;}else{ydir=0;}
	step_motors(abs(x_steps), xdir, params.MOTOR_X_STEP_PIN, params.MOTOR_X_DIR_PIN, abs(y_steps), ydir, params.MOTOR_Y_STEP_PIN, params.MOTOR_Y_DIR_PIN);
}

void manual_movement(){
	ABElectronics_CPP_Libraries::ADCPi pi(params.ADC_ADDRESS_1, params.ADC_ADDRESS_2, params.ADC_BITRATE);
	pi.set_conversion_mode(params.ADC_CONVERSION_MODE);

	float dx = pi.read_voltage(5) - JOYSTICK_VOLTAGE_MIDPOINT;
	float dy = pi.read_voltage(6) - JOYSTICK_VOLTAGE_MIDPOINT;


	cv::Point2f movement_vector = cv::Point2f(dx, dy);

	if(params.DEBUG_MESSAGES){
		std::cout << "dx: " <<movement_vector.x <<" dy: "<<movement_vector.y<<std::endl;
	}
	handle_movement(movement_vector, false);
}


cv::Vec4f normalise_vector(cv::Vec4f v){
	float n1 = v[0] + v[1];
	float n2 = v[2] + v[3];

	v[0] = v[0] / n1;
	v[1] = v[1] / n1;
	v[2] = v[2] / n2;
	v[3] = v[3] / n2;
	
	return v;
}


cv::Vec4f read_reflection_voltage(){
	float v1, v2, v3, v4;

	ABElectronics_CPP_Libraries::ADCPi pi(params.ADC_ADDRESS_1, params.ADC_ADDRESS_2, params.ADC_BITRATE);
	pi.set_conversion_mode(params.ADC_CONVERSION_MODE);

	v1 = pi.read_voltage(1);
	v2 = pi.read_voltage(2);
	v3 = pi.read_voltage(3);
	v4 = pi.read_voltage(4);
	
	cv::Vec4f refelction_vector = cv::Vec4f(v1, v2, v3, v4);

	return normalise_vector(refelction_vector);
}




#endif