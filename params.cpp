#include "params.hpp"

const ConfigParams params("panacea.conf");

ConfigParams::ConfigParams(const std::string &fileName)
:DISPLAY_IMAGE(loadBool(fileName, "GENERAL", "DISPLAY_IMAGE", false))
,DISPLAY_DEBUG_IMAGE(loadBool(fileName, "GENERAL", "DISPLAY_DEBUG_IMAGE", false))
,DEBUG_MESSAGES(loadBool(fileName, "GENERAL", "DEBUG_MESSAGES", false))
,FRAME_DIVIDE(loadInt(fileName, "GENERAL", "FRAME_DIVIDE", 3))
,ADDAPTIVE_THRESH_CORRECTION(loadInt(fileName, "NAVIGATION", "ADDAPTIVE_THRESH_CORRECTION", 1))
,CANNY_MAX_THREASHOLD(loadInt(fileName, "NAVIGATION", "CANNY_MAX_THREASHOLD", 300))
,HOUGH_PROX_RESOLUTION(loadInt(fileName, "NAVIGATION", "HOUGH_PROX_RESOLUTION", 8))
,HOUGH_VOTE_THREASHOLD(loadInt(fileName, "NAVIGATION", "HOUGH_VOTE_THREASHOLD", 50))
,HOUGH_MIN_RADIUS_PERCENT(loadFloat(fileName, "NAVIGATION", "HOUGH_MIN_RADIUS_PERCENT", 0.25))
,VOLTAGE_PIXEL_CONVERSION(loadFloat(fileName, "NAVIGATION", "VOLTAGE_PIXEL_CONVERSION", 100.0f))
,ADC_ADDRESS_1(loadUInt8(fileName, "ADC", "ADC_ADDRESS_1", 0x6f))
,ADC_ADDRESS_2(loadUInt8(fileName, "ADC", "ADC_ADDRESS_2", 0x6f))
,ADC_BITRATE(loadUInt8(fileName, "ADC", "ADC_BITRATE", 16))
,ADC_PGA(loadUInt8(fileName, "ADC", "ADC_PGA", 1))
,ADC_CONVERSION_MODE(loadUInt8(fileName, "ADC", "ADC_CONVERSION_MODE", 1))
,MOTOR_X_STEP_PIN(loadInt(fileName, "GPIO", "MOTOR_X_STEP_PIN", 17))
,MOTOR_X_DIR_PIN(loadInt(fileName, "GPIO", "MOTOR_X_DIR_PIN", 18))
,MOTOR_Y_STEP_PIN(loadInt(fileName, "GPIO", "MOTOR_Y_STEP_PIN", 19))
,MOTOR_Y_DIR_PIN(loadInt(fileName, "GPIO", "MOTOR_Y_DIR_PIN", 22))
,VECTOR_MAG_CONVERSION(loadFloat(fileName, "GPIO", "VECTOR_MAG_CONVERSION", 0.5))
,MANUAL_VECTOR_SENSITIVITY(loadFloat(fileName, "GPIO", "MANUAL_VECTOR_SENSITIVITY", 1.5))
,MOTOR_PULSE_WIDTH_US(loadInt(fileName, "GPIO", "MOTOR_PULSE_WIDTH_US", 100))
{
}

int loadInt(const std::string &fileName, const std::string paramLoc, const std::string paramName, int defautlVal){
	INIReader reader(fileName);
    if (reader.ParseError() != 0) {
        std::cout << "Can't load config file\n";
        exit(-1);
    }

    return reader.GetInteger(paramLoc, paramName, defautlVal);
}

uint8_t loadUInt8(const std::string &fileName, const std::string paramLoc, const std::string paramName, int defautlVal){
    INIReader reader(fileName);
    if (reader.ParseError() != 0) {
        std::cout << "Can't load config file\n";
        exit(-1);
    }

    int val = reader.GetInteger(paramLoc, paramName, defautlVal);

    return (uint8_t) val;
}

float loadFloat(const std::string &fileName, const std::string paramLoc, const std::string paramName, float defautlVal){
	INIReader reader(fileName);
    if (reader.ParseError() != 0) {
        std::cout << "Can't load config file\n";
        exit(-1);
    }

    return reader.GetFloat(paramLoc, paramName, defautlVal);
}

bool loadBool(const std::string &fileName, const std::string paramLoc, const std::string paramName, bool defautlVal){
	INIReader reader(fileName);
    if (reader.ParseError() != 0) {
        std::cout << "Can't load config file\n";
        exit(-1);
    }

    return reader.GetBoolean(paramLoc, paramName, defautlVal);
}

