A HOW TO GUIDE FOR COMPILING, RUNNING AND OPERATING THE ENDOSCOPE

Connection:

Plug the pi into your laptop with the USB C -> C cable. (Connection to a USB A/B port won't work as they cannot provide sufficiant power)

To connect to the pi enter the following command:

ssh -X pi@10.55.0.1

And enter the password: paddington

Once connected to enable GUI intefaces run 

export XAUTHORITY=~/.Xauthority

Note: This requires enabling X11-Forwarding on the host (it is preconfigured on the pi) you may need to find device specific guides to enableing this.


The Endoscope codebase is located at "~/projects/panacea"

Compilation:
Compliling the endoscope codebase is a two step process.
From within the top directory run:
"cmake ."
Then run:
"make"

That will generate an exercutable file.

Running:
To run the endoscope, simple exercuite it with sudo permissions. 

"sudo ./Endoscope"

you can change the mode of operation with the following key presses on an active window:

m -> Change to manual mode. Joystick opperation only
a -> Change to assisted mode. Movement calcualted by navigaton system
v -> Change to View only mode. No movement exercuted just outputs camera stream
q -> Quit the program. 


CONFIGURATION OVERVIEW:
There are several perameters set by the "panacea.conf" file at runtime. These can change the running of the endoscope without re-compilation.

The following is an overview on what each section does.

General: 

DISPLAY_IMAGE : Should a basic image be displayed
DISPLAY_DEBUG_IMAGE : Should the navigation systems step's images be displayed
DEBUG_MESSAGES: Should debug messages be written to std out
FRAME_DIVIDE: only the nth frame defined here is processed. i.e. 1 means that every frame from the camera is processed. 2 would mean that only every other frame is processed. 3, every third frame is processed etc.

NAVIGATION:
This contains the values and threshold the navigation system uses and should not be changed.

ADC:
This contains the values and addresses to connect to the ADC module, and should not be changed.

GPIO:
MOTOR_X_STEP_PIN : The IO pin connected to the x directions step pin
MOTOR_X_DIR_PIN : The IO pin connected to the x directions direction pin
MOTOR_Y_STEP_PIN : The IO pin connected to the y directions step pin
MOTOR_Y_DIR_PIN : the IO pin connected to the y directions direction pin
VECTOR_MAG_CONVERSION : This is a sensitivy measure for the navigation system in terms of motor output
MANUAL_VECTOR_SENSITIVITY : This is a sensitivity measure for the manual controll of the motors (due to noise behaviour above 2.6 is tempremental"
MOTOR_PULSE_WIDTH_US : The pulse width in micro seconds to controll the motor. Lower gives a smoother movement of the motors but has the possibility of skipping steps if put to low.



