cmake_minimum_required(VERSION 3.10)

project(Endescope VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

find_package(OpenCV REQUIRED)
find_package(Threads REQUIRED)
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
find_package(pigpio REQUIRED)

add_executable(Endescope Endescope.cpp params.cpp dependency/ADCPi/ABE_ADCPi.cpp)

include_directories(".")

target_include_directories(Endescope PUBLIC
						   ${header_list}
						   "${PROJECT_SOURCE_DIR}"
						   ${OPENCV_INCLUDE_DIRS}
						   ${pigpio_INCLUDE_DIRS})

target_link_libraries(Endescope ${OpenCV_LIBS} ${pigpio_LIBRARY})
