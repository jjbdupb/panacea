#include <iostream>
#include <opencv2/opencv.hpp>
#include "navigation/handle_navigation.hpp"
#include "cnn/handle_cnn.hpp"
#include "pi_io/IO.hpp"
#include "params.hpp"

extern const ConfigParams params;


enum class Mode{View, Manual, Assisted};

int main(int argc, char const *argv[])
{
	int deviceID = 0;
	int apiID =  cv::CAP_ANY;
	Mode mode;
	mode = Mode::View;

	std::cout << "started frame grabbing" << std::endl;
	cv::Mat frame;
	cv::VideoCapture cap;

	cap.open(deviceID, apiID);

	if (!cap.isOpened()){
		std::cout <<"ERROR: Could not open video stream" << std::endl;
		exit(-1);
	}

	init_IO();

	int count = 0;
	for(;;){
		cap.read(frame);
		count++;
		if(count % params.FRAME_DIVIDE != 0){continue;}

		if(params.DISPLAY_IMAGE){cv::imshow("image", frame);}

		if(mode == Mode::Assisted){
			cv::Point2f direction_vector = handle_navigation(frame);
			handle_movement(direction_vector, true);

		}else if(mode == Mode::Manual){
			manual_movement();
		}else if(mode == Mode::View){
		}
		char input = cv::waitKey(10);


		if(input == 't'){handle_cnn(frame);
		}else if(input=='m'){mode = Mode::Manual;
		}else if(input=='a'){mode = Mode::Assisted;
		}else if(input=='v'){mode = Mode::View;
		}else if(input == 'q'){break;}


	}
	return 0;


}